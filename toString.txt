//Lista PZ i WZ
//Magazynier
public String toString() {
        return 
		//"  id=" + id + '\n' +
        //        "  TrN_TypDokumentu=" + TrN_TypDokumentu + '\n' + // chyba też były odfiltrowane?
                " " + TrN_NumerPelny + '\'' + '\n' +
       //         "  TrN_Bufor=" + TrN_Bufor + '\n' +
       //       "  TrN_Anulowany=" + TrN_Anulowany + '\n' +
       //         "  TrN_PodID=" + TrN_PodID + '\n' +
        //        "  Nazwa='" + Nazwa + '\'' + '\n'  // bo sa już odfiltrowane
		;  
    }

//PDS_Klienty_Lista 

    public String toString() {
        return
           //     "  Klient:"+ '\n' +
            //    "  Knt_KntId = " + Knt_KntId + '\n' +
                Knt_Kod + ' ' +  Nazwa + '\''+ '\n' ;  // będą wiedzieli, że to kod i nazwa
    }


// PDS_Kontenery_Lista MAGAZYNIER, podwidok PZtki 
public String toString() {
        return 
		"ID:"   //docelowo pewnie też do ukrycia ten opis
		+ IdKontenera + '\n' +
             //  "  IdDokumentu = " + IdDokumentu + '\n' +
             //  "  TrN_TypDokumentu = " + TrN_TypDokumentu + '\n' +  //zasadniczo one będe efektem PZ
             //   "  TrN_NumerNr = " + TrN_NumerNr + '\n' +
             //   "  TrN_NumerPelny = '" 
				+ TrN_NumerPelny 
			 //	+ '\'' + '\n' +
             //   "  TrN_Bufor = " + TrN_Bufor + '\n' + 
             //   "  TrN_Anulowany = " + TrN_Anulowany + '\n' +   //anulowanych nie powinno być widać na liście
             //   "  TrN_PodID = " + TrN_PodID + '\n' +
             //   "  NazwaKontrahenta = '" + NazwaKontrahenta + '\'' + '\n' +
              //  "  CzyMoznaWydac = " + CzyMoznaWydac + '\n' +
              //  "  idTowaru = " + idTowaru + '\n' +
              //  "  idMagazynu = " + idMagazynu + '\n' +
               // " rE_TwrNazwa = '" 
				+ TrE_TwrNazwa + 
			//	'\'' + '\n' +
              //  "  TrE_TwrKod = '" + 
			  
			  TrE_TwrKod + '\'' + '\n' +
              TsD_Ilosc + 
			 TrE_Jm + '\'' + '\n' +
              //  "  KodGrupy = '" + KodGrupy + '\'' + '\n' +
             //   "  NazwaGrupy = '" + NazwaGrupy + '\'' + '\n' +
              //  "  TsD_DataOpe = '" + TsD_DataOpe + '\'' + '\n' +
               
              //"  TsD_TrSIdDost = " + TsD_TrSIdDost + '\n' +
              //  "  TsD_Cecha1_Kod = '" + TsD_Cecha1_Kod + '\'' + '\n' +
                "P:" + TsD_Cecha1_Wartosc + 
              //  "  TsD_Cecha2_Kod = '" + TsD_Cecha2_Kod + '\'' + '\n' +
                "K:" + TsD_Cecha2_Wartosc +
              //  "  TsD_Cecha3_Kod = '" + TsD_Cecha3_Kod + '\'' + '\n' +
                "L" + TsD_Cecha3_Wartosc + '\n';
    }
	
	
	// PDS_Kontenery_Lista LINIOWIEC, niezależna lista, widoczna po zalogowaniu 
public String toString() {
        return  IdKontenera + 
               
             
               
         //       "  TrN_Bufor = " + TrN_Bufor + '\n' +
          //      "  TrN_Anulowany = " + TrN_Anulowany + '\n' +   // jesłi anulowany, to ukrywamy
          //      "  TrN_PodID = " + TrN_PodID + '\n' +
              NazwaKontrahenta + '\'' + '\n' +  // WOLAŁBYM KOD, bedzie krócej
              
                "  idMagazynu = " + idMagazynu  +
                "  TrE_TwrNazwa = '" + TrE_TwrNazwa +  +
               // "  TrE_TwrKod = '" + TrE_TwrKod + '\'' + '\n' +
               TsD_Ilosc  + TrE_Jm +'\n' +
               // "  KodGrupy = '" + KodGrupy + '\'' + '\n' +
              //  "  NazwaGrupy = '" + NazwaGrupy + '\'' + '\n' +
             //   "  TsD_DataOpe = '" + TsD_DataOpe + '\'' + '\n' +
               
                //"  TsD_TrSIdDost = " + TsD_TrSIdDost + '\n' +
              //  "  TsD_Cecha1_Kod = '" + TsD_Cecha1_Kod + '\'' + '\n' +
                "P:" + TsD_Cecha1_Wartosc +   //docelowo można schowac literki, najlepiej, żeby wszystko w jednej lini było...
              //  "  TsD_Cecha2_Kod = '" + TsD_Cecha2_Kod + '\'' + '\n' +
                "K:" + TsD_Cecha2_Wartosc +
              //  "  TsD_Cecha3_Kod = '" + TsD_Cecha3_Kod + '\'' + '\n' +
                "L" + TsD_Cecha3_Wartosc + '\n';
    }


//Single_DOK

    public String toString() {
        return 
		//		"  id=" + id + '\n'+
        //        "  TrN_TypDokumentu=" + TrN_TypDokumentu + '\n'+  //jest zawarty w numerze pełnym
                "  TrN_NumerPelny='" + TrN_NumerPelny + '\'' + '\n'+
        //        "  TrN_Bufor=" + TrN_Bufor + '\n'+
         //       "  TrN_Anulowany=" + TrN_Anulowany + '\n'+
                + TrN_DataDok + '\'' + '\n'+
                + TrN_NumerObcy + '\'' + '\n'+
         //       "  TrN_Rodzaj=" + TrN_Rodzaj + '\n'+
         //       "  TrN_PodmiotTyp=" + TrN_PodmiotTyp + '\n'+
          //      "  TrN_PodID=" + TrN_PodID + '\n'+
                + Nazwa + '\'' + '\n'+
                + TrN_Opis + '\'' + '\n'+
                "Zatwierdził: " + TrN_OpeZatwNazwisko + '\'' + '\n'+
                "Wydrukował: " + TrN_OpeWydrNazwisko + '\'' + '\n';
    }

// Single_klient
    public String toString() {
        return  
		//"  Knt_KntId = " + Knt_KntId + '\n' +
               // "  Kod '" + 
				Knt_Kod + 
               // " Nazwa = '" 
				+ Nazwa + '\'' ;
    }


//Single_Kontener
        return 
	IdKontenera + '\n' +
        //        "  ElementofFVId=" + ElementofFVId +'\n' + // zakomentowane moga suię przydać soon
               // "  TsD_TrEId=" + TsD_TrEId +'\n' +
               // "  TsD_TrSIdDost=" + TsD_TrSIdDost +'\n' +
             //   "  IdDokumentu=" + IdDokumentu +'\n' +
               //"  TrN_TypDokumentu=" + TrN_TypDokumentu +'\n' +
              TrN_NumerPelny +'\n' +
              //  "  TrN_Bufor=" + TrN_Bufor +'\n' +
              //  "  TrN_Anulowany=" + TrN_Anulowany +'\n' +
               TrN_DataOpe  +'\n' +
                "Nr Obcy: " + TrN_NumerObcy +'\n' +
               // "  TrN_PodID=" + TrN_PodID +'\n' +
                "  NazwaKontrahenta='" + NazwaKontrahenta + '\'' +'\n' +
                "  CzyMoznaWydac=" + CzyMoznaWydac +'\n' +
               // "  idTowaru=" + idTowaru +'\n' +
                "  idMagazynu=" + idMagazynu +'\n' +
                TrE_TwrNazwa +
                TrE_TwrKod  +'\n' +
				
                "  TrE_TwrOpis='" + TrE_TwrOpis + '\'' +'\n' +
                TsD_Ilosc + TrE_Jm + '\n' +
               // "  KodGrupy='" + KodGrupy + '\'' +'\n' +
              //  "  NazwaGrupy='" + NazwaGrupy + '\'' +'\n' +
                //"  TsD_TrSIdDost = " + TsD_TrSIdDost + '\n' +
              //  "  TsD_Cecha1_Kod = '" + TsD_Cecha1_Kod + '\'' + '\n' +
                "P:" + TsD_Cecha1_Wartosc +   //docelowo można schowac literki, najlepiej, żeby wszystko w jednej lini było...
              //  "  TsD_Cecha2_Kod = '" + TsD_Cecha2_Kod + '\'' + '\n' +
                "K:" + TsD_Cecha2_Wartosc +
              //  "  TsD_Cecha3_Kod = '" + TsD_Cecha3_Kod + '\'' + '\n' +
                "L" + TsD_Cecha3_Wartosc + '\n';
				
				// dojdzie jeszcze:
				//FOTY
				//historia, co kto kiedy robił, ale to w drugiej wersji
				
    }

